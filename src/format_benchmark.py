data = {}
with open("result/benchmark.txt") as in_file:
    for line in in_file:
        if line[:2] == "--": # shows size
            size = int(line.split()[-1])
            data[size] = [[],[],[],[],[],[],[]] # dist: time+mem, clust: time+mem, happiedist: time, happieclust: time+mem
            print size
        elif line[:2] == "Di":
            time = float(line.split()[-2])
            print "Happieclust distance time", time
            data[size][4].append(time)
        else:
            line = line.split(",")
            if len(line) < 2:
                continue
            time = float(line[1].split()[1])
            memory = float(line[4].split()[1])/1024.0
            if line[0] == "-H":
                print "Happieclust",
                data[size][5].append(time)
                data[size][6].append(memory)
            elif line[0] == "-D":
                print "Distance matrix",
                data[size][0].append(time)
                data[size][1].append(memory)
            else:
                print "Clustering",
                data[size][2].append(time)
                data[size][3].append(memory)
            print time, memory
        
print "Size\tDistance calc time\tDistance calc memory\tClustering time\tClustering memory\tHappieclust distance calc time\tHappieclust total time\tHappieclust total memory"
for size, results in sorted(data.items()):
	output = []
	for thing in results:
		output.append(str(sum(thing)/5.0))
	print str(size) + "\t" + "\t".join(output)