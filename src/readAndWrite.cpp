#include <iostream>
#include <fstream>
#include <sstream>
#include "readAndWrite.h"
#include "clustering.h"
using namespace std;

// comment this line to disable debugging output
//#define DEBUG


float* readBinaryDistanceFile(string filename) {
    float *distances;
    
    ifstream::pos_type fileSize;
    ifstream ifs(filename, ios::binary | ios::ate);
    if (ifs.is_open()) {
        fileSize = ifs.tellg();
        S = fileSize / sizeof(disttype);
        ifs.seekg (0, ios::beg);
        distances = new float[fileSize/sizeof(float)];

        for (unsigned int i=0; i < fileSize/sizeof(float); i++) {
            ifs.read((char*)(&distances[i]), sizeof(float));    
        }
    }
    ifs.close();
    
    #ifdef DEBUG
        for (unsigned int i=0; i < fileSize/sizeof(float); i++) {
            cout << distances[i] << endl;
        }
    #endif
    
    return distances;
}

int* readBinarySizesFile(string filename) {
    int *sizes;
    
    ifstream::pos_type fileSize;
    ifstream ifs(filename, ios::binary | ios::ate);
    if (ifs.is_open()) {
        fileSize = ifs.tellg();
        // S = fileSize / sizeof(int);
        ifs.seekg (0, ios::beg);
        sizes = new int[fileSize/sizeof(int)];

        for (unsigned int i=0; i < fileSize/sizeof(int); i++) {
            ifs.read((char*)(&sizes[i]), sizeof(int));    
        }
    }
    ifs.close();
    
    #ifdef DEBUG
        for (unsigned int i = 0; i < fileSize/sizeof(int); i++) {
            cout << sizes[i] << endl;
        }
    #endif
    
    return sizes;
}

void writeBinaryDistanceFile(string filename, float* distances, int nrOfElements) {
    ofstream ofs(filename, ios::binary);
    if (ofs.is_open()) {
        for (int i=0; i < nrOfElements; i++) {
            ofs.write((char*)(&(distances[i])), sizeof(float));
        }
    }
    ofs.close();
}

void writeBinaryCountFile(string filename, int* counts, int nrOfElements) {
    ofstream ofs(filename, ios::binary);
    if (ofs.is_open()) {
        int filler = 0;
        ofs.write((char*)&filler, sizeof(int)); // clustering starts numbering at 1
        for (int i=0; i < nrOfElements; i++) {
            ofs.write((char*)(&(counts[i])), sizeof(int));
        }
    }
    ofs.close();
}

string* readStringFile(string filename, int stringLength, int* nrOfStrings) {
    string* strings;
    
    string line;
    int i = 0;
    ifstream ifs(filename, ios::ate);
    if (ifs.is_open()) {
        ifstream::pos_type fileSize = ifs.tellg();
        ifs.seekg (0, ios::beg);
        #ifdef DEBUG
            cout << "Filesize : " << fileSize << endl;
        #endif
        
        *nrOfStrings = fileSize/(stringLength+1);
        strings = new string[*nrOfStrings];
        
        while (getline (ifs, line)) {
            #ifdef DEBUG
                cout << line << '\n';
            #endif
            strings[i] = line;
            i++;
        }
        ifs.close();
    }
    
    return strings;
}

std::string* readCountAndStringFile(std::string filename, int stringLength, int** counts, int* nrOfStrings) {
    string* strings;
    
    string line;
    int i = 0;
    ifstream ifs(filename, ios::ate);
    if (ifs.is_open()) {
        ifstream::pos_type fileSize = ifs.tellg();
        ifs.seekg (0, ios::beg);
        
        int estimatedNrOfStrings = fileSize/(stringLength+3); // overestimate by max 20% for 12-length strings
        #ifdef DEBUG
            cout << "Filesize : " << fileSize << endl;
            cout << "Estimated nr of strings: " << estimatedNrOfStrings << endl;
        #endif
        strings = new string[estimatedNrOfStrings];
        *counts = new int[estimatedNrOfStrings];
        
        //char* split;
        string buffer;
        while ( getline (ifs, line) ) {
			#ifdef DEBUG
				cout << line << '\n';
			#endif
            stringstream ss(line);
            ss >> buffer;
            //split = strtok(line, "\t"); // is this faster?
            
            #ifdef DEBUG
				cout << "Count " << stoi(buffer);
			#endif
            (*counts)[i] = stoi(buffer);
            // atoi(buffer.c_str()); // is this faster?
            
            //split = strtok(NULL, "\t");
            ss >> buffer;
            #ifdef DEBUG
				cout << ", sequence " << buffer << endl;
			#endif
            strings[i] = buffer;
            
            i++;
        }
        *nrOfStrings = i;
        ifs.close();
    }
    
    return strings;
}

void outputTree(string filename) {
    int i, LH, RH, LHS;
    
    ofstream myfile;
    myfile.open(filename);
    myfile << "# LH\tRH\tLH_SIZE\tRH_SIZE\tDIST\n";
    
    for(i = N - 1; i; i--) {
        LH = MERGE_ORDER_L[i];
        RH = MERGE_ORDER_R[i];
        LHS = MERGE_ORDER_SIZE_L[i];
        myfile << LH << "\t" << RH << "\t" << LHS << "\t" << SIZE[RH] << "\t" << DIST[dist_index(LH, RH)] << "\n";
    }
    
    myfile.close();
}
