#include "hierarchicalClustering.h"
#include "clustering.h"
#include "readAndWrite.h"
#include <iostream>
#include <assert.h>

using namespace std;

int S;
int N;
disttype *DIST;
int *TREE, *SIZE, *MINPTR, *MERGE_ORDER_L, *MERGE_ORDER_R, *MERGE_ORDER_SIZE_L;

int init_clustering(string distance_filename, string sizes_filename) {
  
  int i, j, minj;
  
  DIST = (disttype *)readBinaryDistanceFile(distance_filename);
  SIZE = (int *)readBinarySizesFile(sizes_filename);
  N = calculateN(S);
  
  cout << "Contained " << S << " distances of " << N << "x" << N << " matrix (" << calculateS(N) << ")\n";

  TREE = new int[N+1];
  // SIZE = new int[N+1];
  MINPTR = new int[N+1];
  MERGE_ORDER_L = new int[N+1];
  MERGE_ORDER_R = new int[N+1];
  MERGE_ORDER_SIZE_L = new int[N+1];
  
  for(i = 0; i <= N; i++) {
  	TREE[i] = 0;
  	// SIZE[i] = 1;
  	MINPTR[i] = 0;
  	MERGE_ORDER_L[i] = 0;
  	MERGE_ORDER_R[i] = 0;
  	MERGE_ORDER_SIZE_L[i] = 0;
  }
  
  // calulating index for minimum value for each column
  for(i = 1; i < N; i++) {
  	minj = i + 1;
  	for(j = minj + 1; j <= N; j++) {
  		if(DIST[dist_index(i,j)] < DIST[dist_index(i, minj)]) {
  			minj = j;
  		}
  	}
    MINPTR[i] = minj;
  }
  MINPTR[N] = N;
  
  //delete [] DIST;
  //return DIST;
  return 0;
}

int cluster_to_l() {

	int i;
	// column ID, row ID
	int clust_l, clust_r;
	int countmergers = 0;
	//int dotspermerger = N/100 + 1;
	
	
	clust_l = find_min_distance();
	cout << clust_l << endl;
	while (clust_l) {
	
		clust_r = MINPTR[clust_l];
		
		assert(clust_r <= N);
		
		countmergers++;
		MERGE_ORDER_L[countmergers] = clust_l;
		MERGE_ORDER_R[countmergers] = clust_r;
		MERGE_ORDER_SIZE_L[countmergers] = SIZE[clust_l];
		
		//cout << "Merger\t" << countmergers << "\t" << clust_l << "\t" << clust_r << "\t"
			 //<< DIST[dist_index(clust_l, clust_r)] << "\t-> " << clust_l << endl;
			 
		assert(TREE[clust_l] == 0);
		assert(TREE[clust_r] == 0);
		
		MINPTR[clust_r] = 0; // why r???
		TREE[clust_r] = clust_l;
		
		recalculate_distances(clust_l, clust_r);
		
		for(i = 1; i < clust_l; i++) {
			if (TREE[i]) {
				continue;
			}
			if (MINPTR[i] == clust_l or MINPTR[i] == clust_r) {
				MINPTR[i] = find_minimum_in_column_i(i);
				assert(MINPTR[i]);
				assert(TREE[MINPTR[i]] == 0);
			} else {
				if (DIST[dist_index(i, clust_l)] < DIST[dist_index(i, MINPTR[i])]) {
					MINPTR[i] = clust_l;
				}
				assert(MINPTR[i]);
				assert(TREE[MINPTR[i]] == 0);
			}
			
		}
		
		MINPTR[clust_l] = find_minimum_in_column_i(clust_l);

		for(i = clust_l + 1; i < clust_r; i++) {
			if (TREE[i]) {
				continue;
			}
			if (MINPTR[i] == clust_r) {
				MINPTR[i] = find_minimum_in_column_i(i);
			}
			
		}
		
		assert(TREE[MINPTR[clust_l]] == 0);
		SIZE[clust_l] += SIZE[clust_r];
		
		clust_l = find_min_distance();
	}
	
	cerr << "No more minimal values.\n In total " << countmergers << " merges.\nDONE!" << endl;
	
	return 0;
}

int find_min_distance() {
	
	int i, minimum;
	
	// find first non-negative column minimum
	for(i = 1; i < N and MINPTR[i] == 0; i++);
	
	minimum = i;
	
	// if there were none non-negative column minimums, end
	if (minimum == N) {
		return 0;
	}
	
	// search for the minimum column from next columns from current minimum
	for(i = minimum + 1; i < N; i++) {
		if (MINPTR[i] <= 0) {
			continue;
		}
		
		if (DIST[dist_index(i, MINPTR[i])] < DIST[dist_index(minimum, MINPTR[minimum])]) {
			minimum = i;
		}
	}
	
	return minimum;
}


int find_minimum_in_column_i(int i){
	
	int j, minj;
	float mind;
	
	for(j = i+1; j <= N and TREE[j]; j++);
	
	if (j > N) {
		return 0;
	}
	
	if (TREE[j] == 0) {
		minj = j;
		mind = DIST[dist_index(i, j)];
	} else {
		return 0;
	}
	
	for (j = minj + 1; j <=N; j++) {
		if (TREE[j] == 0) {
			if (DIST[dist_index(i, j)] < mind) {
				minj = j;
				mind = DIST[dist_index(i, j)];
			}
		}
	}
	
	assert(minj);
	assert(TREE[minj] == 0);
	
	return minj;
}


int recalculate_distances(int clust_l, int clust_r) {

	int i, j;
	
	for(i = 1; i < clust_l; i++) {
		if (MINPTR[i]) {
			DIST[dist_index(i, clust_l)] = newdist(i, clust_l, clust_r);
		}
	}
	
	for (j = clust_l + 1; j <= N; j++) {
		if (TREE[j] == 0) {
			DIST[dist_index(clust_l, j)] = newdist(j, clust_l, clust_r);
		}
	}
	
	return 0;
}

float newdist(int k, int i, int j) {
	return ((SIZE[i] * SIZE[k]) * distfun(k, i) + (SIZE[k] * SIZE[j]) * distfun(k, j)) / ((SIZE[i] * SIZE[k]) + (SIZE[k] * SIZE[j]));
}


// why do we need this? ok, probably because we have the lower triangle
float distfun(int i , int j) {
	assert(i != j);
	if (i > j) {
		int t;
		t = i;
		i = j;
		j = t;
	}
	return DIST[dist_index(i,j)];
}

