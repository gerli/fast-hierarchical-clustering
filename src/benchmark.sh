data="data/other/data_for_fhc/GMM21-21_"
#repetitions=1
for size in 500 5000 20000 25000 #100 1000 10000 50000 100000 200000 300000 400000 500000
do
    echo "-----> Working on size $size"
    for rep in 1 2 3 4 5
    do
        echo "Take $rep"
        # This shows all: /usr/bin/time -v
        ## distance calculation, no counts
        #/usr/bin/time -f "-Dnocount,real %e,system %S,avgmemory %K,maxmemory %M,cpuperc %P" bin/distanceTest $data$size.hseq result/${size}_distance_nc.bin
        
        # distance calculation
        /usr/bin/time -f "-D,real %e,system %S,avgmemory %K,maxmemory %M,cpuperc %P" bin/distanceTest $data$size.prot result/${size}_distance.bin result/${size}_count.bin
        # clustering
        /usr/bin/time -f "-C,real %e,system %S,avgmemory %K,maxmemory %M,cpuperc %P" bin/testing result/${size}_tree.out result/${size}_distance.bin result/${size}_count.bin
        # happieclust
        if [ $size -lt 10000 ] 
        then
            /usr/bin/time -f "-H,real %e,system %S,avgmemory %K,maxmemory %M,cpuperc %P" happieclust -i $data$size.nc -o result/${size}.hc -D matrix --output-data -l ave -d l -m 65 --num-distances 242000000 -s 0
        fi
    done
    echo $'\n'
done
