#include <iostream>
#include "distanceMatrix.h"
using namespace std;

//#define DEBUG

int shiftDistance(string s1, string s2) {
	// all penalties are 1
	unsigned int distance = s1.length(); // maximal distance is the length of string
	unsigned int current_distance;
	unsigned int max_compare;
	for (unsigned int shift=0; shift<distance/2; shift++) {
		max_compare = s1.length()-shift;
		// shift left
		current_distance = 2*shift; // insert+delete for each shift
		for (unsigned int i=0; i<max_compare; i++) {
			if (s1.at(i) != s2.at(i+shift)) { // diff
				current_distance++; // substitution penalty 1
			}
		}
		if (current_distance < distance) {
			distance = current_distance;
		}
		
		// shift right
		current_distance = 2*shift; // insert+delete for each shift
		for (unsigned int i=0; i<max_compare; i++) {
			if (s1.at(i+shift) != s2.at(i)) { // diff
				current_distance++; // substitution penalty 1
			}
		}
		if (current_distance < distance) {
			distance = current_distance;
		}
	}
	return distance;
}

float* distanceMatrix (string* strings, int nrOfStrings) {
	float* distances = new float[(nrOfStrings * (nrOfStrings-1)) / 2];
	
	//int pos = 0; // cannot do this in parallel
	#pragma omp parallel for
	for (int j=0; j<nrOfStrings; j++) { // column
		int partialIndex = nrOfStrings*j - (j*(j+1))/2 - j;
		for (int i=j+1; i<nrOfStrings; i++) { // row
			#ifdef DEBUG
				cout << "Comparing " << i << " and " << j << " = "; 
				cout << nrOfStrings*j - (j*(j+1))/2 + i - j - 1 << endl;
			#endif
			distances[partialIndex + i - 1] = (float)shiftDistance(strings[i], strings[j]);
			//pos++;
		}
	}
	return distances;
}
