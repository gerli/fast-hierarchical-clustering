#include <math.h>

#ifndef CLUSTERING_H
#define CLUSTERING_H

typedef float disttype;

extern int N; // nr of elements to cluster
extern int S; // nr of elements in the lower triangle

#define find_column_start(i) 			( N*(i-1) - (i*(i-1))/2 )
#define dist_index(i,j)                 ( find_column_start(i) + j-i-1 )
#define calculateS(N)          			((N*N-N)/2)
#define calculateN(size)        		(((int)sqrt( 1 + 8*size) / 2)+1)

extern disttype *DIST; /* Address of the first element d(1,2) of distance matrix */
extern int *TREE, *SIZE, *MINPTR, *MERGE_ORDER_L, *MERGE_ORDER_R, *MERGE_ORDER_SIZE_L;
#endif
