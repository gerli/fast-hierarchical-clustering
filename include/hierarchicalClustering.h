#include <sstream>

#ifndef HIERARCHICALCLUSTERING_H
#define HIERARCHICALCLUSTERING_H


int init_clustering(std::string distance_filename, std::string sizes_filename);

int cluster_to_l();

int find_min_distance();

int find_minimum_in_column_i(int i);

int recalculate_distances(int clust_l, int clust_r);

float newdist(int k, int i, int j);

float distfun(int i , int j);

#endif
