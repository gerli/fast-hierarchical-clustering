#include <string>
#ifndef READANDWRITE_H
#define READANDWRITE_H


float* readBinaryDistanceFile(std::string filename);
int* readBinarySizesFile(std::string filename);

void writeBinaryDistanceFile(std::string filename, float* distances, int nrOfElements);
void writeBinaryCountFile(std::string filename, int* counts, int nrOfElements);

std::string* readStringFile(std::string filename, int stringLength, int* nrOfStrings);
std::string* readCountAndStringFile(std::string filename, int stringLength, int **counts, int* nrOfStrings);

void outputTree(std::string filename);

#endif
