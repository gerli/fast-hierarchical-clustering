#include <iostream>
#include <sstream>
#include <cstdlib>
#include <list>
#include <vector>
#include <algorithm>
#include <fstream>
#include "readAndWrite.h"
#include "hierarchicalClustering.h"
#include "clustering.h"

using namespace std;

int main (int argc, char* argv[]) {
    // 1. argument: output file for tree [optional]
    // 2. argument: input file for distance matrix [optional, with 3rd]
    // 3. argument: input file for counts [optional, with 2nd]
    try {
        string tree_out = "result/tree.out";
        string distances = "result/distance.bin";
        string counts = "result/count.bin";
        
        switch(argc) {
            case 1:
                cout << "Using default output file: " << tree_out << endl;
                cout << "Using default input files: " << distances << " and " << counts << endl;
                break;
            case 2: 
                tree_out = argv[1];
                break;
            case 4:
                tree_out = argv[1];
                distances = argv[2];
                counts = argv[3];
                break;
            default: 
                cout << "Using default input files: " << distances << " and " << counts << endl;
                break;
        }
        
        //init_clustering("/home/mari-liis/Documents/programs/fastHC/example.bin", "/home/mari-liis/Documents/programs/fastHC/sizes.bin");
        init_clustering(distances, counts);
        cluster_to_l();
        outputTree(tree_out);
        //outputTree("/home/mari-liis/Documents/programs/fastHC/tree.out");
        
        
        
        delete [] DIST;
        delete [] TREE;
        delete [] SIZE;
        delete [] MINPTR;
        delete [] MERGE_ORDER_L;
        delete [] MERGE_ORDER_R;
        delete [] MERGE_ORDER_SIZE_L;

    } catch (string& str) {
        cout << "Error: " << str << endl;
    }
    
    return EXIT_SUCCESS;
}
