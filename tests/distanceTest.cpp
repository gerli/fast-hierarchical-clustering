#include <iostream>
#include "readAndWrite.h"
#include "distanceMatrix.h"
using namespace std;

// comment this line to disable debugging output
//#define DEBUG
// comment this line to disable progress output
#define VERBOSE

int main (int argc, char* argv[]) {
    // 1. argument: input file
    // 2. argument: output file for distance matrix [optional, with 3rd]
    // 3. argument: output file for counts [optional, with 2nd]
    // 4. argument: input file contains counts // TODO
    try {
        string input;
        string output_distance = "result/distance.bin";
        string output_count = "result/count.bin";
        bool use_counts = true;
        
        switch(argc) {
            case 1: 
                cout << "No input file specified!" << endl;
                return EXIT_FAILURE;
            case 2: 
                input = argv[1];
                break;
            case 3:
                input = argv[1];
                output_distance = argv[2];
                use_counts = false;
                cout << "Not using count files!" << endl;
                break;
            case 4:
                input = argv[1];
                output_distance = argv[2];
                output_count = argv[3];
                break;
            default: 
                cout << "Using default output files: " << output_distance << " and " << output_count << endl;
                break;
        }
        
        //cout << input << " " << output_distance << " " << output_count << endl;
        
        //string input = "data/GMM25-18_10.prot";
        //string output_distance = "result/GMM25-18_10_distance.bin";
        //string output_count = "result/GMM25-18_10_count.bin";
        
        //string input = "data/other/GMM25-18_100.prot";
        //string input = "data/other/data_for_fhc/GMM21-21_100.prot";
        
        int nrOfStrings;
        
        // Read data
        int* counts;
        #ifdef VERBOSE
            cout << "Reading data..." << endl;
        #endif
        
        string* data;
        if (use_counts) {
            data = readCountAndStringFile(input, 12, &counts, &nrOfStrings);
        } else {
            data = readStringFile(input, 12, &nrOfStrings);
        }
        
        #ifdef DEBUG
            for (int i=0; i<nrOfStrings; i++) {
                cout << i << " " << data[i] << " " << counts[i] << endl;
            }
            cout << "Number of strings: " << nrOfStrings << endl;
        #endif
        
        // Write count file
        if (use_counts) {
            #ifdef VERBOSE
                cout << "Writing counts to " << output_count << "..." << endl;
            #endif
            writeBinaryCountFile(output_count, counts, nrOfStrings);
            delete[] counts; // no need for count file anymore
        }
        
        // Calculate distance matrix
        #ifdef VERBOSE
            cout << "Calculating distance matrix..." << endl;
        #endif
        float* distances = distanceMatrix(data, nrOfStrings);
        
        #ifdef DEBUG
            for (int i=0; i<(nrOfStrings * (nrOfStrings - 1))/2; i++) {
                cout << distances[i] << " ";
            }
            cout << endl;
        #endif
        
        // Write distance matrix
        #ifdef VERBOSE
            cout << "Writing distance matrix to " << output_distance << "..." << endl;
        #endif
        writeBinaryDistanceFile(output_distance, distances, (nrOfStrings * (nrOfStrings - 1))/2);
        
        #ifdef DEBUG
            // Read distance matrix
            float* dist = readBinaryDistanceFile(output_distance);
            for (int i=0; i<(nrOfStrings * (nrOfStrings - 1))/2; i++) {
                cout << dist[i] << " ";
            }
            cout << endl;
        #endif
        
    } catch (string& str) {
        cout << "Error: " << str << endl;
    }

    return EXIT_SUCCESS;
}
