#####################################################
# You may need to change the parameters under here. #
#####################################################

# Step 1: Choose a compiler. By default, use clang++

# If you use clang++, make sure the following line does not start with a comment (#)
#CXX=clang++
# If you use g++, uncomment the following line
CXX=g++

# Set default compiler parameters
# -Wall 	shows all warnings when compiling, always use this!
# -std=c++11 	enables the C++11 standard mode
CXXFLAGS = -Wall -std=c++11 

# Enable parallel stuff
ENDFLAGS = -fopenmp

# Step 2: If you use gcc 4.6, we need different parameters. Uncomment the following line.
#CXXFLAGS = -Wall -std=c++0x

# Step 3: If you use clang++ under Mac OS X, remove these comments
#CXXFLAGS += -stdlib=libc++
#LFLAGS += -stdlib=libc++

# Step 4: Run 'make' in the same directory as this file


############################
# Settings for the library #
############################


# Compiler flag -Idir specifies, that there are includes in the 'dir' directory
LIB_CXXFLAGS = $(CXXFLAGS) -Iinclude

# List of objects for the library
LIBOBJS = obj/readAndWrite.o obj/hierarchicalClustering.o obj/clustering.o obj/distanceMatrix.o

# Name of the library
LIBRARY = lib/libmytest.a

################################
# Settings for the testing app #
################################

# Compiler flags for the testing app
APP_CXXFLAGS = $(CXXFLAGS) -Iinclude

# Linker flags (order the compiler to link with our library)
LFLAGS += -Llib -lmytest

# The object for the testing app
TESTOBJS = obj/testing.o
DISTTESTOBJS = obj/distanceTest.o

# The name of the testing app
TESTAPP = bin/testing
DISTTESTAPP = bin/distanceTest

# This is the first target. It will be built when you run 'make' or 'make all'
all: $(LIBRARY)

# Create the library by using 'ar'
$(LIBRARY) : $(LIBOBJS)
	ar cr $(LIBRARY) $(LIBOBJS)

# Compile each source file of the library
obj/readAndWrite.o: src/readAndWrite.cpp
	$(CXX) $(LIB_CXXFLAGS) -c src/readAndWrite.cpp -o obj/readAndWrite.o $(ENDFLAGS)

obj/hierarchicalClustering.o: src/hierarchicalClustering.cpp
	$(CXX) $(LIB_CXXFLAGS) -c src/hierarchicalClustering.cpp -o obj/hierarchicalClustering.o $(ENDFLAGS)

obj/clustering.o: src/clustering.cpp
	$(CXX) $(LIB_CXXFLAGS) -c src/clustering.cpp -o obj/clustering.o $(ENDFLAGS)
	
obj/distanceMatrix.o: src/distanceMatrix.cpp
	$(CXX) $(LIB_CXXFLAGS) -c src/distanceMatrix.cpp -o obj/distanceMatrix.o $(ENDFLAGS)

# Rule for linking the test app with our library
$(TESTAPP): $(TESTOBJS) $(LIBRARY)
	$(CXX) $(TESTOBJS) -o $(TESTAPP) $(LFLAGS) $(ENDFLAGS)
	
$(DISTTESTAPP): $(DISTTESTOBJS) $(LIBRARY)
	$(CXX) $(DISTTESTOBJS) -o $(DISTTESTAPP) $(LFLAGS) $(ENDFLAGS)

# Compile each source file of the library
obj/testing.o: tests/testing.cpp
	$(CXX) $(APP_CXXFLAGS) -c tests/testing.cpp -o obj/testing.o $(ENDFLAGS)

obj/distanceTest.o: tests/distanceTest.cpp
	$(CXX) $(APP_CXXFLAGS) -c tests/distanceTest.cpp -o obj/distanceTest.o $(ENDFLAGS)

test: $(TESTAPP) $(DISTTESTAPP)

doc:
	doxygen

clean:
	rm -f $(LIBOBJS)
	rm -f $(TESTOBJS)
	rm -f $(LIBRARY)
	rm -f $(TESTAPP)
	rm -f $(DISTTESTAPP)
	rm -f -r docs/*
